##VIRTU CONNEXION
```bash
$ ssh dattier.iutinfo.fr
```

#INTO PHYS MACHINE

##SSH KEYS

###Keys Creation
```bash
$ ssh-keygen
```
Your key is stock in this file
```bash
/home/infoetu/login/.ssh/id_rsa
```

###Add your Key to your serveur
1.Copy and transmit the key to your serveur
```bash
$ ssh-copy-id 
…
$ ssh-copy-id -i ~/.ssh/id_rsa.pub dattier.iutinfo.fr
```

#INTO THE VIRTUAL MACHINE

##Creation and start

1.Creation
```bash
$ vmiut creer matrix
```

2.Start
```bash
$ vmiut demarrer nom_machine
```

3.List of the machine
```bash
$ vmiut lister
```

4.Stop
```bash
$ vmiut arreter nom_machine
```

5.Delete
```bash
$ vmiut supprimer nom_machine
```

6.Information
```bash
$ vmiut info nom_machine
```

7.Redirect the graphics interface
```bash
$ ssh -X virtu
```

8.Connection
```bash
$ vmiut console nom_machine
```

9.With ssh
```bash
$ ssh user@10.42.xx.yy
```

10.Reboot
```bash
$ reboot
```

###Root

1.Log in user in the VM
```bash
$ ssh user@10.42.xx.yy
```

2.Pass in root
```bash
$ su -
```
3.Open /etc/ssh/ssh_config
```bash
$ nano /etc/ssh/ssh_config
```

4.Replace #PubkeyAuthentication yes by PubkeyAuthentication yes
```bash
$ PubkeyAuthentication yes
```

##CONFIG IP FOR VM

###IP and Gateway
1.Open /etc/network/interfaces 
```bash
$ nano /etc/network/interfaces 
```

2.Put the address and gateway
```bash
# The primary network interface
allow-hotplug enp0s3

iface enp0s3 inet static
    	address your address
    	gateway your gateway
    	dsn-nameservers 10.42.0.1
```

3.Reboot
```bash
$ reboot
```

#UPDATE
```bash
$ apt update && apt full-upg
```