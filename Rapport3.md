(sur la vm matrix)

## 1.1 Serveur HTTP

1.Install nginx
```bash
$ sudo apt update
$ sudo apt install nginx
```

2.Start nginx
```bash
$ sudo apt install nginx
```

3.Verify
```bash
$ sudo systemctl status nginx
```
4. Install curl
```bash
$ sudo apt update
$ sudo apt install curl
```

4. Verify
```bash
$ curl http://localhost
```

## 1.2
Ce n'est pas possible d'y accéder directement car par défault la VM est "isolé" sur sur son propre réseaux et sous réseaux et n'est pas accesible par défault depuis la machine physique.

(Depuis dattier)
1. Connection to matrix
```bash
$ ssh -L 9090:localhost:9090 matrix@10.42.138.2
```

2. Config 
```bash
nano ~/.ssh/config

Host matrix
    HostName 10.42.138.2
    Port 22
    User matrix
    LocalForward 9090 localhost:9090
```

(depuis la VM)

## 2 Synapse

2.1 Installation
sudo apt install -y lsb-release wget apt-transport-https
sudo wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ $(lsb_release -cs) main" |
    sudo tee /etc/apt/sources.list.d/matrix-org.list
sudo apt update
sudo apt install matrix-synapse-py3

Pensez à mettre "nom-machine-phys:8008" en remplçant le nom de la machine par la votre pour l'installation de la VM

2.2 Config
sudo systemctl stop matrix-synapse
sudo nano /etc/matrix-synapse/homeserver.yaml
bind_addresses: ['::1', '127.0.0.1', '10.42.?.?']

Remplacer l'adresse par une adresse disponible sur le réseau 10.42
sudo systemctl start matrix-synapse


2.3
sudo systemctl stop matrix-synapse
sudo nano /etc/matrix-synapse/homeserver.yaml
trusted_key_servers: []
sudo systemctl start matrix-synapse

2.4 Use posgres for synapse
Connect to posgres :
sudo -u postgres psql

Create the BDD for synapse in posgres
CREATE DATABASE synapse;
CREATE USER synapse_user WITH ENCRYPTED PASSWORD 'votre_mot_de_passe';
ALTER DATABASE synapse OWNER TO synapse_user;

Quit posgres
\q

Go to homeserver.yaml and put this config

database:
  name: psycopg2
  args:
    user: synapse_user
    password: votre_mot_de_passe
    database: synapse
    host: localhost
    cp_min: 5
    cp_max: 10




