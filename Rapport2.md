## ALIAS

1.Open ~/.ssh/config
```bash
$ nano ~/.ssh/config
```
2.Put this information in config
```bash
 1 Host virt
 2 HostName dattier.iutinfo.fr
 3 User joshua.collin.etu
 4 Port 22
```

3.Transfer of agent for ssh since phys
```bash
$ scp -r ~/.ssh dattier.iutinfo.fr:~/
```




Partie 2

## Hostname

### Display Current Hostname: 
```bash
hostname
```

### Edit Hostname

1.Open /etc/hostname
```bash
$ nano /etc/hostname
```

2. Remplace ‘debian’ by what you want
```txt
$ matrix
```

3. Save file
```txt
Ctrt + O and Ctrl + X
```

4. Open /etc/hosts
```bash
$ nano /etc/hosts
```

5. Change the old hostname to new
```bash
127.0.0.1   	localhost
127.0.1.1   	matrix

# The following lines are desirable for IPv6 capable hosts
::1 	localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

6. Apply Change
```bash
$ reboot
```

## Install and Configure Sudo

### Installation
1.Run 
```bash
$ apt update 
***
$ apt install sudo
```

### Configuration

1. Add a Sudo user

```bash
$ usermod -aG sudo user
```

2. Try cmd

```bash
sudo whoami
```

## Install and Configure PostGresQL

### Installation

1. add apt-key
```bash
$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
```

2. install from apt
```bash
$ sudo apt install postgresql-15
```
reply Y to question

3. 
$ sudo pg_createcluster 15 main --start

4. Verify 
$ sudo systemctl status postgresql@15-main

5. Start
$ sudo service postgresql start

6. Connection
$ sudo -u postgres psql

7. Create user matrix
CREATE USER matrix WITH PASSWORD 'matrix';

8. Create BDD
CREATE DATABASE matrix OWNER matrix;

9. Connection with matrix
psql -U matrix -d matrix -h localhost -p 5432

